package com.thechalakas.jay.cursoradapter;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity
{

    EditText text1, text2;
    DatabaseHelper dbHelp;
    ListView alist;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("MainActivity","onCreate Reached Start");

        text1 = (EditText) findViewById(R.id.EText1);
        text2 = (EditText) findViewById(R.id.EText2);
        dbHelp = new DatabaseHelper(this);
        alist = (ListView) findViewById(R.id.newlist);
        // alist = getListView();
        getData();

        Log.i("MainActivity","onCreate Reached End");
    }


    public void addNew(View view)
    {
        Log.i("MainActivity","addNew Reached Start");

        String s1 = text1.getText().toString();
        String s2 = text2.getText().toString();
        dbHelp.addData(s1, s2);
        getData();

        Log.i("MainActivity","addNew Reached End");
    }

    public void getData()
    {
        Log.i("MainActivity","getData Reached Start");

        Cursor cursor = dbHelp.fetchData();

        ListAdapter myAdapter = new SimpleCursorAdapter(this, R.layout.tasks,
                cursor,
                new String[]{dbHelp._ID, dbHelp.COLUMN_1, dbHelp.COLUMN_2},
                new int[]{R.id.idnum, R.id.c1, R.id.c2}, 0);

        alist.setAdapter(myAdapter);

        Log.i("MainActivity","getData Reached End");
    }
}
